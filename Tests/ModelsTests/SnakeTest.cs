﻿using App.Controllers;
using App.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace Tests.ModelsTests
{
    [TestClass]
    public class SnakeTest
    {
        [TestMethod]
        public void InitialDirectionTest()
        {
            var snake = new Snake();
            Assert.AreEqual( snake.Direction, IGameObject.Direction.None );
        }


        [TestMethod]
        public void InitialLengthTest()
        {
            var snake = new Snake();
            Assert.AreEqual( snake.Body.Count, 1 );
        }


        [TestMethod]
        public void MoveUpTest()
        {
            var snake = new Snake();
            snake.MoveUp();
            Assert.AreEqual( snake.Direction, IGameObject.Direction.Up );
        }

        [TestMethod]
        public void MoveDownTest()
        {
            var snake = new Snake();
            snake.MoveDown();
            Assert.AreEqual( snake.Direction, IGameObject.Direction.Down );
        }

        [TestMethod]
        public void MoveLeftTest()
        {
            var snake = new Snake();
            snake.MoveLeft();
            Assert.AreEqual( snake.Direction, IGameObject.Direction.Left );
        }

        [TestMethod]
        public void MoveRightTest()
        {
            var snake = new Snake();
            snake.MoveRight();
            Assert.AreEqual( snake.Direction, IGameObject.Direction.Right );
        }


        [TestMethod]
        public void UpdateTest()
        {
            var snake = new Snake();

            snake.MoveDown();
            snake.Update();

            Assert.IsTrue( snake.Body.First.Value.PositionY == GamePropertiesHolder.Instance.SnakeStartingPositionY + 1);
        }


        [TestMethod]
        public void EatTest()
        {
            var snake = new Snake();
            var apple = new StubApple();

            snake.Eat( apple );
            var first = snake.Body.First.Value;

            Assert.IsTrue( first.PositionX == apple.PositionX && first.PositionY == apple.PositionY );
            Assert.AreEqual( snake.Body.Count, 2 );
        }


        [TestMethod]
        public void IsAliveTrueAtStartTest()
        {
            var snake = new Snake();

            Assert.IsTrue( snake.IsAlive );
        }


        [TestMethod]
        public void IsAliveTrueWhenEatApple()
        {
            var snake = new Snake();

            snake.Eat( new StubApple() );
            Assert.IsTrue( snake.IsAlive );
        }


        [TestMethod]
        public void IsAliveFalseWhenEatBody()
        {
            var snake = new Snake();

            snake.Eat( snake.Body.First.Value );

            Assert.IsFalse( snake.IsAlive );
        }
    }


    class StubApple : IGameObject
    {
        public int PositionX { get => 2; set { } }
        public int PositionY { get => 2; set { } }
    }
}
