﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using App.Models;


namespace Tests.ModelsTests
{
    [TestClass]
    public class PlayerTest
    {
        [TestMethod]
        public void InitialScoreEqualZeroTest()
        {
            var player = new Player( "test" );

            Assert.AreEqual( 0, player.Score );
        }


        [TestMethod]
        public void InitialLevelEqualOneTest()
        {
            var player = new Player( "test" );

            Assert.AreEqual( 1, player.Level );
        }
    }
}
