﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using App.Models;
using App.Controllers;


namespace Tests.ModelsTests
{
    [TestClass]
    public class AppleTest
    {
        [TestMethod]
        public void RandomPositionTest()
        {
            var apple = new Apple();
            var differentApple = new Apple();

            Assert.AreNotEqual( apple, differentApple );
        }


        [TestMethod]
        public void RandomPositionFitsInRangeTest()
        {
            var apple = new Apple();

            Assert.IsTrue( apple.PositionX < GamePropertiesHolder.Instance.BoardSizeX + GamePropertiesHolder.Instance.BoardPositionOffset
                && apple.PositionY < GamePropertiesHolder.Instance.BoardSizeY + GamePropertiesHolder.Instance.BoardPositionOffset );
        }
    }
}
