﻿namespace App.Controllers
{
    public class GamePropertiesHolder
    {
        private GamePropertiesHolder() { }


        public readonly int BoardSizeX = 30;

        public readonly int BoardSizeY = 15;


        public int SnakeStartingPositionX
        {
            get => (BoardSizeX + BoardPositionOffset) / 2;
        }

        public int SnakeStartingPositionY
        {
            get => (BoardSizeY + BoardPositionOffset) / 2;
        }


        public readonly int BoardPositionOffset = 2;


        public static GamePropertiesHolder Instance
        {
            get
            {
                if( instance is null )
                {
                    instance = new GamePropertiesHolder();
                }
                return instance;
            }
        }

        private static GamePropertiesHolder instance;
    }
}
