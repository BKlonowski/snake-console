﻿using App.Models;
using System;


namespace App.Controllers
{
    public class UserInput
    {
        public IGameObject.Direction Key
        {
            get => Console.KeyAvailable ? ConsoleKeyToDirection() : IGameObject.Direction.None;
        }


        private IGameObject.Direction ConsoleKeyToDirection()
        {
            return Console.ReadKey( intercept: true ).Key switch
            {
                ConsoleKey.UpArrow => IGameObject.Direction.Up,
                ConsoleKey.DownArrow => IGameObject.Direction.Down,
                ConsoleKey.LeftArrow => IGameObject.Direction.Left,
                ConsoleKey.RightArrow => IGameObject.Direction.Right,
                _ => IGameObject.Direction.None,
            };
        }
    }
}
