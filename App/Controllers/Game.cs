﻿using System;
using System.Collections.Generic;
using System.Linq;
using App.Models;


namespace App.Controllers
{
    public class Game
    {
        public Game( ref Snake snake, ref List<Apple> apples )
        {
            snakeReference = snake;
            applesReference = apples;
            SpawnApple();
        }


        public void Update()
        {
            var input = new UserInput();

            CheckGameObjectsCollision();
            MoveSnake( input.Key );
        }


        public bool IsFinished
        {
            get => !snakeReference.IsAlive;
        }



        private void MoveSnake( IGameObject.Direction direction )
        {
            switch( direction )
            {
                case IGameObject.Direction.Up:
                    snakeReference.MoveUp();
                    break;

                case IGameObject.Direction.Down:
                    snakeReference.MoveDown();
                    break;

                case IGameObject.Direction.Left:
                    snakeReference.MoveLeft();
                    break;

                case IGameObject.Direction.Right:
                    snakeReference.MoveRight();
                    break;

                case IGameObject.Direction.None:
                default:
                    break;
            }

            snakeReference.Update();
        }


        internal int CalculateSpeedByLevel( int level )
        {
            return 500 - level * 40;
        }


        internal int CalculateScore()
        {
            return snakeReference.Body.Count;
        }



        private void CheckGameObjectsCollision()
        {
            var snakeHead = snakeReference.Body.First.Value;
            var snakeBody = snakeReference.Body.Skip( 1 ).ToList();

            var appleColliding = applesReference.Find( apple => apple.PositionX == snakeHead.PositionX && apple.PositionY == snakeHead.PositionY );
            if( appleColliding != null )
            {
                snakeReference.Eat( appleColliding );
                applesReference.Remove( appleColliding );
                SpawnApple();
            }
            else if( snakeBody != null && snakeBody.Exists( element => element.PositionX == snakeHead.PositionX && element.PositionY == snakeHead.PositionY) )
            {
                snakeReference.Eat( snakeHead );
            }
        }


        private void SpawnApple()
        {
            var newApple = new Apple();
            applesReference.Add( newApple );
        }



        private Snake snakeReference;
        private List<Apple> applesReference;
    }
}
