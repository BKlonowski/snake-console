﻿using System;
using System.Threading;
using App.Controllers;
using App.Models;
using App.Views;
using System.Collections.Generic;


namespace App
{
    class Program
    {
        static void Main( string[] args )
        {
            Console.CursorVisible = false;

            var snake = new Snake();
            var apples = new List<Apple>();

            var game = new Game( ref snake, ref apples );
            var view = new ConsoleView( ref snake, ref apples );

            view.DisplayWelcome();
            Console.ReadKey();
            var player = new Player( view.GetPlayerNameFromUser() );

            while( !game.IsFinished && player.Level < 10 )
            {
                game.Update();
                player.Score = game.CalculateScore();
                view.Render();
                view.DisplayPlayerInfo( player );
                Thread.Sleep( game.CalculateSpeedByLevel( player.Level ) );
            }

            view.DisplayGoodbye();
            Console.ReadLine();
        }
    }
}
