﻿namespace App.Models
{
    public class Player
    {
        public Player( string name ) => this.name = name;


        public int Level
        {
            get => score / 5 + 1;
        }


        public int Score
        {
            get => score;
            set
            {
                if( value > score )
                    score = value;
            }
        }


        public string Name
        {
            get => name;
        }


        private int score;

        private readonly string name;
    }
}
