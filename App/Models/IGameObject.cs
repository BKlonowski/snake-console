﻿using System;


namespace App.Models
{
    public interface IGameObject
    {
        public int PositionX
        {
            get;
            set;
        }


        public int PositionY
        {
            get;
            set;
        }


        public enum Direction
        {
            Up,
            Down,
            Left,
            Right,

            None
        }
    }
}
