﻿using App.Controllers;
using System;
using System.Collections.Generic;


namespace App.Models
{
    public class Snake
    {
        public Snake()
        {
            Direction = IGameObject.Direction.None;
            Body = new LinkedList<SnakeElement>();
            Body.AddFirst( new SnakeElement(
                GamePropertiesHolder.Instance.SnakeStartingPositionX,
                GamePropertiesHolder.Instance.SnakeStartingPositionY ) );

            isAlive = true;
        }


        public void MoveUp() => Direction = IGameObject.Direction.Up;

        public void MoveDown() => Direction = IGameObject.Direction.Down;

        public void MoveLeft() => Direction = IGameObject.Direction.Left;

        public void MoveRight() => Direction = IGameObject.Direction.Right;


        public void Update()
        {
            Body.AddFirst( GetFirstByDirection() );
            Body.RemoveLast();
        }


        public void Eat( IGameObject apple )
        {
            isAlive = !Body.Contains( apple as SnakeElement );

            var newElement = new SnakeElement( apple );
            Body.AddFirst( new LinkedListNode<SnakeElement>(newElement) );
        }



        public LinkedList<SnakeElement> Body
        {
            get;
            private set;
        }


        public IGameObject.Direction Direction
        {
            get;
            private set;
        }


        public bool IsAlive
        {
            get => isAlive && IsInBorders();
        }


        private bool IsInBorders()
        {
            var head = Body.First.Value;
            var props = GamePropertiesHolder.Instance;

            return head.PositionX < props.BoardSizeX + props.BoardPositionOffset && head.PositionX >= props.BoardPositionOffset &&
                   head.PositionY < props.BoardSizeY + props.BoardPositionOffset && head.PositionY >= props.BoardPositionOffset;
        }


        private SnakeElement GetFirstByDirection()
        {
            var first = new SnakeElement( Body.First.Value as SnakeElement );
            switch( Direction )
            {
                case IGameObject.Direction.Up:
                    first.PositionY--;
                    break;

                case IGameObject.Direction.Down:
                    first.PositionY++;
                    break;

                case IGameObject.Direction.Left:
                    first.PositionX--;
                    break;

                case IGameObject.Direction.Right:
                    first.PositionX++;
                    break;

                case IGameObject.Direction.None:
                default:
                    break;
            }
            return first;
        }


        private bool isAlive;
    }



    public class SnakeElement : IGameObject
    {
        public SnakeElement( IGameObject element )
        {
            PositionX = element.PositionX;
            PositionY = element.PositionY;
        }


        public SnakeElement( int positionX, int positionY )
        {
            PositionX = positionX;
            PositionY = positionY;
        }


        public int PositionX
        {
            get;
            set;
        }


        public int PositionY
        {
            get;
            set;
        }
    }
}
