﻿using App.Controllers;
using System;


namespace App.Models
{
    public class Apple : IGameObject
    {
        public Apple()
        {
            PositionX = (new Random()).Next(
                GamePropertiesHolder.Instance.BoardPositionOffset,
                GamePropertiesHolder.Instance.BoardSizeX + GamePropertiesHolder.Instance.BoardPositionOffset );
            PositionY = (new Random()).Next(
                GamePropertiesHolder.Instance.BoardPositionOffset,
                GamePropertiesHolder.Instance.BoardSizeY + GamePropertiesHolder.Instance.BoardPositionOffset );
        }


        public int PositionX
        {
            get;
            set;
        }

        public int PositionY
        {
            get;
            set;
        }
    }
}
