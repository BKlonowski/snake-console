﻿using System;
using System.Collections.Generic;
using System.Linq;
using App.Controllers;
using App.Models;


namespace App.Views
{
    public class ConsoleView
    {
        public ConsoleView( ref Snake snake, ref List<Apple> apples )
        {
            snakeReference = snake;
            applesReference = apples;
        }


        public void DisplayWelcome()
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine( "Snake - the console version game!" );

            Console.ResetColor();
            Console.WriteLine( $"Welcome!\nRules are simple:\n" +
                $"1. Do not cross the borders!\n" +
                $"2. Do not eat yourself!\n" +
                $"3. Speed will increase along with the level - be prepared\n" );

            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine( "Controls:" );
            Console.ResetColor();
            Console.WriteLine( $"Left:\t{ConsoleKey.LeftArrow}\n" +
                $"Right:\t{ConsoleKey.RightArrow}\n" +
                $"Up:\t{ConsoleKey.UpArrow}\n" +
                $"Down:\t{ConsoleKey.DownArrow}" );
            Console.ResetColor();
        }


        public void DisplayGoodbye()
        {
            ClearScreen();
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine( "Thank you for playing!\n" );
            Console.ResetColor();
            Console.WriteLine( $"Your score:\t{snakeReference.Body.Count * 3}\n" +
                $"Your level:\t" );
        }


        public void Render()
        {
            ClearScreen();
            PrintBorders();
            PrintApples();
            PrintSnake();
        }


        public string GetPlayerNameFromUser()
        {
            Console.Write( "Enter your name:" );
            return Console.ReadLine();
        }


        public void DisplayPlayerInfo( Player player )
        {
            PrintResults( player );
        }


        private void PrintSnake()
        {
            PrintSnakeHead();
            PrintSnakeBody();
        }


        private void PrintApples()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            foreach( var apple in applesReference )
            {
                Console.SetCursorPosition( apple.PositionX, apple.PositionY );
                Console.Write( "*" );
            }
            Console.ResetColor();
        }


        private void ClearScreen()
        {
            Console.Clear();
        }


        private void PrintSnakeHead()
        {
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.SetCursorPosition( snakeReference.Body.First.Value.PositionX, snakeReference.Body.First.Value.PositionY );
            Console.Write( "O" );
            Console.ResetColor();
        }


        private void PrintSnakeBody()
        {
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            foreach( var snakeBodyElement in snakeReference.Body.Skip(1) )
            {
                Console.SetCursorPosition( snakeBodyElement.PositionX, snakeBodyElement.PositionY );
                Console.Write( "O" );
            }
            Console.ResetColor();
        }


        private void PrintBorders()
        {
            Console.BackgroundColor = ConsoleColor.DarkGray;
            PrintHorizontalBorders();
            PrintVerticalBorders();
            Console.ResetColor();
        }


        private void PrintVerticalBorders()
        {
            var settings = GamePropertiesHolder.Instance;
            for( int verticalIndex = 1; verticalIndex < settings.BoardSizeY + settings.BoardPositionOffset + 1; ++verticalIndex )
            {
                Console.SetCursorPosition( 1, verticalIndex );
                Console.Write( " " );
                Console.SetCursorPosition( settings.BoardSizeX + settings.BoardPositionOffset, verticalIndex );
                Console.Write( " " );
            }
        }


        private void PrintHorizontalBorders()
        {
            var settings = GamePropertiesHolder.Instance;
            for( int horizontalIndex = 1; horizontalIndex < settings.BoardSizeX + settings.BoardPositionOffset; ++horizontalIndex )
            {
                Console.SetCursorPosition( horizontalIndex, 1 );
                Console.Write( " " );
                Console.SetCursorPosition( horizontalIndex, settings.BoardSizeY + settings.BoardPositionOffset );
                Console.Write( " " );
            }
        }


        private void PrintResults( Player player )
        {
            var resultsPositionX = GamePropertiesHolder.Instance.BoardSizeX + 5;

            Console.ForegroundColor = ConsoleColor.White;

            Console.SetCursorPosition( resultsPositionX, 1 );
            Console.Write( "Snake - The console version game!" );
            Console.SetCursorPosition( resultsPositionX, 2 );
            Console.Write( $"Player:\t{player.Name}" );
            Console.SetCursorPosition( resultsPositionX, 4 );
            Console.Write( $"Score:\t{player.Score}" );
            Console.SetCursorPosition( resultsPositionX, 6 );
            Console.Write( $"Length:\t{snakeReference.Body.Count}" );
            Console.SetCursorPosition( resultsPositionX, 8 );
            Console.Write( $"Level:\t{player.Level}" );

            Console.ResetColor();
        }


        private Snake snakeReference;

        private List<Apple> applesReference;
    }
}
