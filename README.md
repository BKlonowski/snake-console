# Snake-Console #
Console version of Snake game, but with fancy colors!


---
## Purpose ##
The main idea behind this project was to check the ability of C# language with .NET Core and LINQ when it comes to rapid development.

Plus, who doesn't like to play Snake?


---
## Technology ##
As already mentioned this project was developed using the following tools:

|      Name     | Version |
|:-------------:|:-------:|
|       C#      |   8.0   |
|   .NET Core   |   3.1   |
| Visual Studio |  16.5.4 |


---
## Development ##
The whole development process was based on a single *master* branch.

More branches would only be used in this project for refactoring, risky bugfixing, etc.

This approach had a chance of being successful only because of one developer working on the project.

With proper commit messages and consequent commiting after each step it was possible to still keep the trackability and an order in development.


---
## Result ##

The application is launched in the default console window of the system.

It has some colors applied to the standard output.

The game window looks just like presented below:

![Snake-Console example](Pics/MainGameScreen.png "Example play of Snake-Console")


---
## How to play ##

1. To play the game it's required to download the built Release package from *Downloads* section of the repository.  
To start downloading the package from here, just click [this link](https://bitbucket.org/BKlonowski/snake-console/downloads/Snake-Console.zip).

2. After downloading is complete go to the download directory and find the downloaded package packed with default Windows archive format.  
Unpack it and launch the *Snake.exe* application with the project icon.

3. It's also possible to create the shortcut of *Snake.exe* file to any directory.

4. When the game starts the main welcome screen is displayed with all the rules and controls keys/buttons explained.  
Press Enter only after getting familiar with those rules.  
After that please put your name and just hit Enter.  
The game will start, but the Snake won't move until any W/A/S/D button is pressed for the first time.

5. The game continues level by level, until level 10 is reached.


Have fun!

